package ip.dashboard.datapump;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 * Session Bean implementation class DataPump
 */
@Stateful(mappedName = "DataPump")
@LocalBean
public class DataPump {

    /**
     * Default constructor. 
     */
    public DataPump() {
        // TODO Auto-generated constructor stub
    }
    @PostConstruct
    void StartUp(){System.out.println("DataPump Created");}
    

}
