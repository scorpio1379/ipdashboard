package ip.dashboard.datapump.zabbix;

import ip.dashboard.datapump.DataPump;
import ip.dashboard.datapump.zabbix.api.ZApi;
import ip.dashboard.datapump.zabbix.api.ZApi.Zapi;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Session Bean implementation class ZDataPump
 */
@Stateful(mappedName = "ZDataPump")
@LocalBean
public class ZDataPump extends DataPump {
     private static String zLogin;
     private static String zPassword;
     private static String zUrl;
     private static String zAuth;
     private ZApi zApi;
     
    /**
     * @throws Exception 
     * @see DataPump#DataPump()
     */
    public ZDataPump() throws Exception {
        super();
        this.init();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws Exception{
    	zApi = (ZApi) new InitialContext().lookup("java:module/ZApi");
    	if ( (zLogin!= null) && (zUrl != null)) {
    		ZDataPump.zAuth = zApi.getAuth(zLogin,zPassword,zUrl);
    	} else {
    		ZDataPump.zAuth = zApi.getAuth(zLogin,zPassword,zUrl);
    		ZDataPump.zAuth = "";
    	}
    }
    
    public void  getHosts(){
    	
    }
    
    public void getItems(String zHostId){
    	
    }

}
