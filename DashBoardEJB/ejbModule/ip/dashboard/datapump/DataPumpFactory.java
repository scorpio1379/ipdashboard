package ip.dashboard.datapump;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Session Bean implementation class DataPumpFactory
 */
@Singleton(mappedName = "DataPumpFactory")
@LocalBean
public class DataPumpFactory {

    /**
     * Default constructor. 
     */
    public DataPumpFactory() {
        // TODO Auto-generated constructor stub
    }
    @PostConstruct
    void StartUp(){System.out.println("DataPumpFactory Created");}
    
    public DataPump getDataPumpInstance() throws Throwable{
    	return (DataPump) new InitialContext().lookup("java:app/DashBoardEJB/DataPump");
    }
    public DataPump getDataPumpInstance(DataPumpType dpt) throws Exception {
    	if (dpt == DataPumpType.Zabbix ) {
    		return (DataPump) new InitialContext().lookup("java:app/DashBoardEJB/ZDataPump");
    	}else {
    		throw new Exception ("NOT IMPLEMENTED");
    	}
    }

}
