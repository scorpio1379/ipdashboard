package ip.dashboard;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;

import ip.dashboard.datapump.DataPump;
import ip.dashboard.datapump.DataPumpFactory;
import ip.dashboard.datapump.DataPumpType;

/**
 * Session Bean implementation class DashBoardBean
 */
@ApplicationScoped
@Startup
@Singleton(mappedName = "DashBoard")
public class DashBoardBean implements DashBoard {
	//@javax.inject.Inject
	//DataPumpFactory dpFactory;

    /**
     * Default constructor. 
     * @throws Throwable 
     */
    public DashBoardBean() throws Throwable {
        // TODO Auto-generated constructor stub
    	DataPumpFactory dpFactory = (DataPumpFactory) new InitialContext().lookup("java:module/DataPumpFactory");
    	DataPump dpi = dpFactory.getDataPumpInstance();
    	System.out.println("DataPump Inited" + dpi.getClass().getSimpleName());
    	DataPump zdpi = dpFactory.getDataPumpInstance(DataPumpType.Zabbix);
    	System.out.println("DataPump Inited" + zdpi.getClass().getSimpleName());
    }
    @PostConstruct
    void StartUp(){
    	System.out.println("DashBoard is Started UP");
    }

}
